jQuery( document ).ready(function( $ ){
	jQuery(".curitiba_nerd .row").owlCarousel({
		loop:true,
		autoplay:true,
		nav: true,
		navText: 	['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive:{
			0:{
				items:2
			},
			480:{
				items:3
			}
		}
	});	

	jQuery(".ultimas_criticas .row").owlCarousel({
		loop:true,
		autoplay:true,
		nav: true,
		navText: 	['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive:{
			0:{
				items:2
			},
			480:{
				items:5
			}
		}
	});		


	jQuery(".canal_youtube .row").owlCarousel({
		loop:true,
		autoplay:true,
		nav: true,
		navText: 	['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive:{
			0:{
				items:2
			},
			480:{
				items:5
			}
		}
	});		
});