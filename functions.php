<?php

show_admin_bar(false);

add_theme_support( 'post-thumbnails' );
add_image_size("post-highlight", 500, 290, true );

function theme_scripts() {
	# CSS
	wp_enqueue_style ( 'oswald', 'https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700');
  wp_enqueue_style ( 'oswald', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');

	wp_enqueue_style ( 'font-awesome', get_template_directory_uri() . '/_assets/css/font-awesome.min.css' );
	wp_enqueue_style ( 'main', get_template_directory_uri() . '/_assets/css/all.min.css' );
	wp_enqueue_style ( 'hover', get_template_directory_uri() . '/_assets/css/hover.css' );
  wp_enqueue_style ( 'animate', get_template_directory_uri() . '/_assets/css/animate.css' );
   wp_enqueue_style ( 'owl-carrosel-css', get_template_directory_uri() . '/_assets/css/owl.carousel.min.css' );
  wp_enqueue_style ( 'hamburgers-animate', get_template_directory_uri() . '/_assets/css/hamburgers.min.css' );
  wp_enqueue_style ( 'owl-carrosel-css-default', get_template_directory_uri() . '/_assets/css/owl.theme.default.min.css' );
  wp_enqueue_style ( 'owl-carrosel-css-green', get_template_directory_uri() . '/_assets/css/owl.theme.green.min.css' );
	
	# JS
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'js', get_template_directory_uri() . '/_assets/js/app.min.js');
    wp_enqueue_script( 'cookie', get_template_directory_uri() . '/_assets/js/jquery.cookie.js');
    wp_enqueue_script( 'owl-carrosel-js', get_template_directory_uri() . '/_assets/js/owl.carousel.min.js');

	wp_localize_script(
		'jquery',
		'print',
		array(
			'template'		=> get_bloginfo('template_url'), 
			'url' 			=> get_bloginfo('url')
		)
	);
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );


$pages = array( "Notícias", "Games", "Livros | HQs", "Filmes", "Series", "CuritibaNerd","últimas Críticas" );
foreach ($pages as $p) {
	$page = get_page_by_title($p, "", "page" );
	if (!$page) {
		wp_insert_post(array(
			'post_content'   => "",
			'post_title'     => $p,
			'post_status'    => 'publish',
			'post_type'      => 'page'
		));
	}
}

function limitarTexto($texto, $limite){
    $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
    return $texto;
}

function custom_admin() {
	wp_enqueue_script( 'datatable-js', '//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js');
	wp_enqueue_style ( 'datatable-css', '//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css' );
	wp_enqueue_style ( 'custom-admin', get_template_directory_uri() . '/_assets/css/admin.min.css' );
}
add_action('admin_enqueue_scripts', 'custom_admin');
add_action('login_enqueue_scripts', 'custom_admin');

function wp_custom_breadcrumbs() {
 
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '/'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'categoria "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
}

function bg_page(){
?>
<?php if(get_field('bg_page', get_the_ID()) != null){ ?>
  <section class="title_page">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p><?php echo wp_custom_breadcrumbs(); ?></p>
          <p class="title"><?php the_title(); ?></p>          
        </div>        
      </div>
    </div>
  </section>
  <?php }else if(get_post_type() == 'produtos'){ ?>
  <section class="bg_page" style="background-image: url('<?php echo get_field("bg_page", 71); ?>');">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p>Produtos</p>
          <p><?php echo wp_custom_breadcrumbs(); ?></p>
        </div>        
      </div>
    </div>
  </section>
  <?php }else if(is_category()){ ?>
  <section class="bg_page" style="background-image: url('<?php echo get_field("bg_page", 71); ?>');">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p>Blog</p>
          <p><?php echo wp_custom_breadcrumbs(); ?></p>
        </div>        
      </div>
    </div>
  </section>  
<?php }else{ ?>	
	<section class="title_page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p><?php echo wp_custom_breadcrumbs(); ?></p>
					<p class="title"><?php the_title(); ?></p>					
				</div>				
			</div>
		</div>
	</section>
<?php
	}//fim else
}

function list_categories_post_type($post, $id){

  $customPostTaxonomies = get_object_taxonomies($post);

  if(count($customPostTaxonomies) > 0)
  {
    foreach($customPostTaxonomies as $tax)
    {
      $args = array(
      'orderby' => 'name',
      'child_of'=> $id,
      'show_count' => 0,
      'pad_counts' => 0,
      'hierarchical' => 1,
      'taxonomy' => $tax,
      'title_li' => ''
      );
      wp_list_categories( $args );
    }
  }
}


function twentyfifteen_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Widget Area', 'twentyfifteen' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_videos-blog',
    'title' => 'Videos Blog',
    'fields' => array (
      array (
        'key' => 'field_5a2ae5715960e',
        'label' => 'Video Um',
        'name' => 'video_um',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5c65960f',
        'label' => 'Video Dois',
        'name' => 'video_dois',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5dc59629',
        'label' => 'Video Tres',
        'name' => 'video_tres',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59628',
        'label' => 'Video Quatro',
        'name' => 'video_quatro',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59627',
        'label' => 'Video Cinco',
        'name' => 'video_cinco',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59626',
        'label' => 'Video Seis',
        'name' => 'video_seis',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59625',
        'label' => 'Video Sete',
        'name' => 'video_sete',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59624',
        'label' => 'Video Oito',
        'name' => 'video_oito',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59623',
        'label' => 'Video Nove',
        'name' => 'video_nove',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5db59622',
        'label' => 'Video Dez',
        'name' => 'video_dez',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da59621',
        'label' => 'Video Onze',
        'name' => 'video_onze',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da59620',
        'label' => 'Video Doze',
        'name' => 'video_doze',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da5961f',
        'label' => 'Video Treze',
        'name' => 'video_treze',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da5961e',
        'label' => 'Video Quatorze',
        'name' => 'video_quatorze',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da5961d',
        'label' => 'Video Quinze',
        'name' => 'video_quinze',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5da5961c',
        'label' => 'Video Dezesseis',
        'name' => 'video_dezesseis',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d95961b',
        'label' => 'Video Dezessete',
        'name' => 'video_dezessete',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d95961a',
        'label' => 'Video Dezoito',
        'name' => 'video_dezoito',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d959619',
        'label' => 'Video Dezenove',
        'name' => 'video_dezenove',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d959618',
        'label' => 'Video Vinte',
        'name' => 'video_vinte',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d959617',
        'label' => 'Video Vinte e Um',
        'name' => 'video_vinte_e_um',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d859616',
        'label' => 'Video Vinge e Dois',
        'name' => 'video_vinte_e_dois',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d859615',
        'label' => 'Video Vinte e Tres',
        'name' => 'video_vinte_e_tres',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d859614',
        'label' => 'Video Vinte e Quatro',
        'name' => 'video_vinte_e_quatro',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d859613',
        'label' => 'Video Vinte e Cinco',
        'name' => 'video_vinte_e_cinco',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d859612',
        'label' => 'Video Vinte e Seis',
        'name' => 'video_vinte_e_seis',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d759611',
        'label' => 'Video Vinte e Sete',
        'name' => 'video_vinte_e_sete',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae5d659610',
        'label' => 'Video Vinte e Oito',
        'name' => 'video_vinte_e_oito',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae7955962c',
        'label' => 'Video Vinte e Nove',
        'name' => 'video_vinte_e_nove',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_5a2ae79f5962d',
        'label' => 'Video Trinta',
        'name' => 'video_trinta',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'page',
          'operator' => '==',
          'value' => '23',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'side',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
