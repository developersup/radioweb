<!DOCTYPE html>
<html lang="pt">
<head>
	
	<title><?php echo wp_title(" | "); ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="theme-color" content="#ffffff">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/icon/favicon.ico">
	<link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/icon/favicon.ico" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/icon/favicon.ico" sizes="64x64">
	<link rel="mask-icon" href="<?php bloginfo("template_url"); ?>/_assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/_assets/css/owl.carousel.min.css">
	<script src="<?php echo get_bloginfo('template_url'); ?>/_assets/css/owl.carousel.min.css"></script>

	<?php
		wp_head();		
	?>
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->	
</head>

<?php 
	global $post;
	$slug = get_post( $post )->post_name;
?>

<style type="text/css">
	
</style>

<body class="<?php if(is_front_page()){ echo "home";}else{echo 'internas';} ?>">
	<input id="slug" style="visibility: hidden; position: absolute;top: 0;left: 0;z-index: 0;" value="<?php echo $slug; ?>">

	<script type="text/javascript">
		var slug_page = jQuery("#slug").val();
	</script>
<header>
	<section class="menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-5">
					<figure>
						<a href="<?php echo get_bloginfo('url'); ?>">
							<img src="<?php echo get_bloginfo('template_url') ?>/_assets/img/logo/logo.png">
						</a>
					</figure>					
				</div>
				<div class="col-lg-9 col-md-9 col-sm-7 menu_desk">					
					<?php wp_nav_menu( array('menu' => 'primary', 'menu_class' => 'boots_wp_nav') ); ?>
				  <div class="hamburger hamburger--collapse">
				    <div class="hamburger-box">
				      <div class="hamburger-inner"></div>
				    </div>
				  </div>						
					<i class="fa fa-search open_search" aria-hidden="true"></i>
					<form class="search-box" action="<?php bloginfo('url'); ?>" method="GET">
						<input type="text" name="s" class="form-control" placeholder="O que você procura?">
						<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
						<i class="fa fa-times search_close" aria-hidden="true"></i>	
					</form>				
				</div>
			</div>
			<div class="row mobile_menu">
				<?php wp_nav_menu( array('menu' => 'primary', 'menu_class' => 'boots_wp_nav') ); ?>				
			</div>
		</div>	
	</section>
</header>