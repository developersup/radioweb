<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_url( $thumb_id );
?>
<div class="col-lg-3 col-md-6 col-sm-6" style="margin-top: 2%;">
	<figure>
		<img src="<?php echo $thumb_url; ?>">
	</figure>
	<div class="info">
		<h3><?php the_title(); ?></h3>
		<p><?php the_excerpt(); ?></p>
		<a href="<?php the_permalink(); ?>" class="hvr-wobble-horizontal">Leia Mais</a>		
	</div>
</div>