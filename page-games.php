<?php get_header(); ?>
<?php the_post(); ?>
<?php bg_page(); ?>

<section class="noticias">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				<p>GAMES, JOGOS NERDNATION, POKEMON, MARVEL, PS3, XBOX, NINTENDO, WII PLAYSTATION, MICROSOFT, STEAM ENTRE OUTROS</p>
			</div>
			
		</div>		
		<div class="row posts">
			<?php
				// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' 	=> 'post',
					'order'     	=> 'DESC',
					'category_name'  => 'games',
					'posts_per_page'=> '200',
					// 'paged'         => $paged,
					'post_status'	=> 'publish'
				);
				$wc_query = new WP_Query( $args );

	            if ($wc_query -> have_posts()):
	                while ($wc_query -> have_posts()): $wc_query -> the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );	
						$cat = get_the_category( get_the_ID() );	                	
			?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-12 no-padding-right">
				<a href="<?php the_permalink(); ?>">
					<div class="artigo">					
						<article>
							<figure>
								<img src="<?php echo $thumb_url; ?>">
							</figure>
							<div class="desc">
								<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
								<h3><?php the_title(); ?></h3>								
							</div>				
						</article>
					</div>
				</a>
			</div>
			<?php
				endwhile;				
				endif;
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>